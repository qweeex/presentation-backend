"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
var Config_json_1 = require("../data/Config.json");
exports["default"] = (function () {
    return function (req, res, next) {
        if (req.method === "OPTIONS") {
            next();
        }
        try {
            if (req.headers.authorization) {
                var token = req.headers.authorization.split(" ")[1];
                if (!token) {
                    return res.json({
                        status: false,
                        message: "Токен не найден"
                    });
                }
                // @ts-ignore
                var currentUser = jsonwebtoken_1["default"].verify(token, Config_json_1.secret);
                if (currentUser.userID) {
                }
                else {
                    return res.json({
                        status: false,
                        message: "Ошибка проверки токена"
                    });
                }
                next();
            }
            else {
                return res.json({ status: false, message: "Ошибка проверки токена" });
            }
        }
        catch (error) {
            return res.json({ status: false, message: "Глобальная ошибка проверки прав", error: error });
        }
    };
});
