"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var MysqlManager_1 = __importDefault(require("../manager/MysqlManager"));
var WebManager_1 = __importDefault(require("../manager/WebManager"));
var SlidesController = /** @class */ (function () {
    function SlidesController() {
    }
    SlidesController.GetPresentation = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var presentationID;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        presentationID = req.params.id;
                        if (!presentationID) return [3 /*break*/, 2];
                        return [4 /*yield*/, MysqlManager_1["default"].Instance.MysqlPoolConnections.query('SELECT * FROM `PRESENTATION` WHERE `ID_PRESENTATION` = ?', presentationID, function (err, results) {
                                if (err) {
                                    console.log(err);
                                }
                                else {
                                    res.json({
                                        status: true,
                                        data: results[0]
                                    });
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        res.json({
                            status: false,
                            message: "Presentaiton not found"
                        });
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    SlidesController.GetAllPresentation = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var userID;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        userID = req.body.userID;
                        if (!userID) return [3 /*break*/, 2];
                        return [4 /*yield*/, MysqlManager_1["default"].Instance.MysqlPoolConnections.query('SELECT * FROM `PRESENTATION` WHERE `ID_USER` = ?', userID, function (err, results) {
                                if (err) {
                                    console.log(err);
                                }
                                else {
                                    res.json({
                                        status: true,
                                        data: results
                                    });
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        res.json({
                            status: false,
                            message: "Presentaiton not found"
                        });
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    SlidesController.SavePresentation = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = req.body;
                        if (!(data.presentationID != null)) return [3 /*break*/, 2];
                        return [4 /*yield*/, MysqlManager_1["default"].Instance.MysqlPoolConnections.query('UPDATE  PRESENTATION SET ? WHERE ID_PRESENTATION = ?', [
                                {
                                    'DATEMODIFY': new Date(),
                                    'NAME': data.name,
                                    'STATUS': data.status,
                                    'SLIDES': JSON.stringify(data.slides),
                                    'ID_SLIDE': data.activeSlide,
                                    'ID_USER': data.userID
                                },
                                data.presentationID
                            ], function (err, results) {
                                if (err) {
                                    console.log(err);
                                }
                                else {
                                    return res.json({
                                        status: true,
                                        results: results
                                    });
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 4];
                    case 2: return [4 /*yield*/, MysqlManager_1["default"].Instance.MysqlPoolConnections.query('INSERT INTO PRESENTATION SET ?', {
                            'NAME': data.name,
                            'DATEMODIFY': new Date(),
                            'CREATEDDATE': new Date(),
                            'STATUS': data.status,
                            'SLIDES': JSON.stringify(data.slides),
                            'ID_SLIDE': data.activeSlide,
                            'ID_USER': data.userID
                        }, function (err, results) {
                            if (err) {
                                console.log(err);
                            }
                            else {
                                console.log(results);
                                return res.json({
                                    status: true,
                                    presentationID: results.insertId
                                });
                            }
                        })];
                    case 3:
                        _a.sent();
                        _a.label = 4;
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    SlidesController.GetVote = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var presentationID;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        presentationID = req.params.id;
                        if (!presentationID) return [3 /*break*/, 2];
                        return [4 /*yield*/, MysqlManager_1["default"].Instance.MysqlPoolConnections.query('SELECT * FROM `PRESENTATION` WHERE `ID_PRESENTATION` = ?', presentationID, function (err, results) {
                                if (err) {
                                    console.log(err);
                                }
                                else {
                                    var data = results[0];
                                    var CurrentSlide_1 = data.ID_SLIDE;
                                    var slides = JSON.parse(data.SLIDES).filter(function (item) { return item.id == CurrentSlide_1; });
                                    res.render('pages/answer', {
                                        layout: 'layouts/default',
                                        info: slides,
                                        presentId: presentationID,
                                        CurrentSlide: CurrentSlide_1
                                    }, (function (err, html) { return WebManager_1["default"].Instance.OnRequestHaveRender(req, res, err, html); }));
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        res.json({
                            status: false,
                            message: "Presentation not found"
                        });
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    SlidesController.SaveVote = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = req.body;
                        if (!data) return [3 /*break*/, 2];
                        return [4 /*yield*/, MysqlManager_1["default"].Instance.MysqlPoolConnections.query('INSERT INTO PRESENTATION_ANSWER SET ?', {
                                'ID_SLIDE': data.id_slide,
                                'ID_QUESTION': data.id_question,
                                'ANSWER': data.answer,
                                'ID_PRESENTATION': data.id_presentation,
                                'CHARTS': data.charts
                            }, function (err, results) {
                                if (err) {
                                    console.log(err);
                                }
                                else {
                                    return res.json({
                                        status: true,
                                        results: results
                                    });
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        res.json({
                            status: false,
                            message: "Data not found"
                        });
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    SlidesController.GetResults = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var data;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        data = req.body;
                        if (!data) return [3 /*break*/, 2];
                        return [4 /*yield*/, MysqlManager_1["default"].Instance.MysqlPoolConnections.query('SELECT ANSWER, count(ANSWER), CHARTS FROM `PRESENTATION_ANSWER` WHERE ID_SLIDE = ? AND ID_PRESENTATION = ?  GROUP BY ANSWER', [
                                data.id_slide,
                                data.id_presentation
                            ], function (err, results) {
                                if (err) {
                                    console.log(err);
                                    return res.json({
                                        status: false,
                                        err: err
                                    });
                                }
                                else {
                                    var data_1 = [];
                                    var labels_1 = [];
                                    var wc_1 = [];
                                    results.forEach(function (item) {
                                        if (item.CHARTS === 'cloud') {
                                            wc_1.push({
                                                name: item.ANSWER,
                                                value: item['count(ANSWER)'],
                                                charts: item.CHARTS
                                            });
                                        }
                                        else {
                                            data_1.push(item['count(ANSWER)']);
                                            labels_1.push(item.ANSWER);
                                        }
                                    });
                                    if (wc_1.length > 0) {
                                        return res.json({
                                            status: true,
                                            type: "wc",
                                            data: wc_1
                                        });
                                    }
                                    else {
                                        return res.json({
                                            status: true,
                                            type: "any",
                                            data: {
                                                data: data_1,
                                                labels: labels_1
                                            }
                                        });
                                    }
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        res.json({
                            status: false,
                            message: "Data not found"
                        });
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    return SlidesController;
}());
exports["default"] = SlidesController;
