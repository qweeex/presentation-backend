"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var WebManager_1 = __importDefault(require("./manager/WebManager"));
WebManager_1["default"].Instance.Start();
