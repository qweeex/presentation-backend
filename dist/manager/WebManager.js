"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var express_1 = __importDefault(require("express"));
var http_1 = require("http");
var socket_io_1 = require("socket.io");
var cors_1 = __importDefault(require("cors"));
var express_ejs_layouts_1 = __importDefault(require("express-ejs-layouts"));
var SlidesController_1 = __importDefault(require("../controller/SlidesController"));
var AuthController_1 = __importDefault(require("../controller/AuthController"));
var path_1 = __importDefault(require("path"));
var fs_1 = __importDefault(require("fs"));
var CheckAuth_1 = __importDefault(require("../middleware/CheckAuth"));
var WebManager = /** @class */ (function () {
    function WebManager() {
        this.PORT = 8089;
        this.App = express_1["default"]();
        this.App.use(express_1["default"].json());
        this.App.use(express_ejs_layouts_1["default"]);
        this.App.disable('x-powered-by');
        this.App.set("view engine", "ejs");
        this.App.set('layout', 'layouts/default');
        this.App.use(cors_1["default"]());
        this.server = http_1.createServer(this.App);
        this.io = new socket_io_1.Server(this.server, {
            cors: {
                origin: '*'
            }
        });
        this.ApiRouter();
    }
    WebManager.prototype.ApiRouter = function () {
        // Auth API
        this.App.post('/api/auth/login', AuthController_1["default"].LoginUser);
        this.App.post('/api/auth/registration', AuthController_1["default"].RegistrationUser);
        this.App.post('/api/presentation', CheckAuth_1["default"](), SlidesController_1["default"].GetAllPresentation); // Сохранение презентации
        this.App.post('/api/presentation/save', CheckAuth_1["default"](), SlidesController_1["default"].SavePresentation); // Сохранение презентации
        this.App.post('/api/presentation/:id', CheckAuth_1["default"](), SlidesController_1["default"].GetPresentation); // Получить презентацию по ID
        this.App.get('/preview/:id', SlidesController_1["default"].GetVote); // Ответы на вопросы
        this.App.post('/preview/save', SlidesController_1["default"].SaveVote); // Передача ответа на вопрос
        this.App.post('/api/results', SlidesController_1["default"].GetResults);
        this.App.use('*', WebManager.OnErrorRouteRequest);
    };
    WebManager.prototype.Start = function () {
        var _this = this;
        this.server.listen(this.PORT, function () {
            console.log("Server start on port " + _this.PORT);
        });
        this.io.on("connect", function (socket) {
            console.log("Connected client on port %s.", _this.PORT);
            socket.on("message", function (m) {
                console.log("[server](message): %s", JSON.stringify(m));
                _this.io.emit("message", m);
            });
            socket.on('init', function (m) { return console.log(m); });
            socket.on("disconnect", function () {
                console.log("Client disconnected");
            });
        });
    };
    WebManager.OnErrorRouteRequest = function (req, res) {
        var url = decodeURI(req.baseUrl);
        var currentPath = path_1["default"].resolve('./static' + url);
        if (fs_1["default"].existsSync(currentPath)) {
            res.sendFile(currentPath);
        }
        else {
            res.status(404).send('err 404');
        }
    };
    WebManager.prototype.OnRequestHaveRender = function (req, res, err, html) {
        if (err) {
            console.log(err);
            res.render("pages/errors/500", {
                layout: 'layouts/error'
            }, function (errFinish, HtmlFinish) {
                if (errFinish) {
                    res.statusCode = 500;
                    res.end("Fatal error, please again later!" + "<!--" + String(err) + "-->");
                }
                res.end(HtmlFinish + "<!--" + String(err) + "-->");
            });
        }
        else {
            res.end(html);
        }
    };
    WebManager.Instance = new WebManager();
    return WebManager;
}());
exports["default"] = WebManager;
