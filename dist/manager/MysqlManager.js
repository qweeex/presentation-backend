"use strict";
exports.__esModule = true;
// @ts-ignore
var mysql_1 = require("mysql");
var MysqlManager = /** @class */ (function () {
    function MysqlManager() {
        // https://splinter.beget.com/phpMyAdmin
        this.MysqlCurrentSetting = {
            host: 'slygerev.beget.tech',
            user: 'slygerev_bd',
            pass: '3J7m%xj0',
            base: 'slygerev_bd'
        };
        this.MysqlPoolConnections = mysql_1.createPool({
            connectionLimit: 10,
            host: this.MysqlCurrentSetting.host,
            user: this.MysqlCurrentSetting.user,
            password: this.MysqlCurrentSetting.pass,
            database: this.MysqlCurrentSetting.base,
            charset: 'utf8mb4'
        });
        this.MysqlPoolConnections.on('error', function (err) {
            console.error(err);
        });
        this.MysqlPoolConnections.query('SET NAMES utf8mb4');
    }
    MysqlManager.Instance = new MysqlManager();
    return MysqlManager;
}());
exports["default"] = MysqlManager;
