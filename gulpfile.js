const gulp = require('gulp')
const sass = require('gulp-sass')(require('sass'))
const browserSync = require('browser-sync').create()
const autoprefixer = require('gulp-autoprefixer');
const babel = require('gulp-babel');
const concat = require('gulp-concat');




gulp.task('scss', () => {
    return gulp.src("src/assets/scss/index.scss")
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest("static/assets/css"));
});

gulp.task('libs', () => {
    return gulp.src(['src/assets/js/libs/*.js'])
        .pipe(concat('libs.js'))
        .pipe(gulp.dest('static/assets/js/'));
});

gulp.task('scripts', () => {
    return gulp.src(['src/assets/js/*.js'])
        .pipe(concat('bundle.js'))
        .pipe(gulp.dest('static/assets/js/'));
});


gulp.task('build', gulp.series('scss', 'libs', 'scripts'))

gulp.task('serve', () => {

    gulp.series('scss', 'libs', 'scripts')

    gulp.watch("src/assets/js/*.js", gulp.series('scripts'))
    gulp.watch("src/assets/js/*.js").on('change', gulp.series('scripts'))

    gulp.watch("src/assets/js/libs/*.js", gulp.series('libs'))
    gulp.watch("src/assets/js/libs/*.js").on('change', gulp.series('libs'))

    gulp.watch("src/assets/scss/**/*.scss", gulp.series('scss'))
    gulp.watch("src/assets/scss/**/*.scss").on('change', gulp.series('scss'))



})