import express from "express";
import MysqlManager from "../manager/MysqlManager";
import WebManager from "../manager/WebManager";

class SlidesController {

    public static async GetPresentation(req: express.Request, res: express.Response){
        let presentationID = req.params.id
        if (presentationID){
            await MysqlManager.Instance.MysqlPoolConnections.query('SELECT * FROM `PRESENTATION` WHERE `ID_PRESENTATION` = ?', presentationID, (err: Error, results: any) => {
                if (err){
                    console.log(err)
                } else {
                    res.json({
                        status: true,
                        data: results[0]
                    })
                }
            })
        } else {
            res.json({
                status: false,
                message: "Presentaiton not found"
            })
        }
    }

    public static async GetAllPresentation(req: express.Request, res: express.Response){
        let {userID} = req.body
        if (userID){
            await MysqlManager.Instance.MysqlPoolConnections.query('SELECT * FROM `PRESENTATION` WHERE `ID_USER` = ?', userID, (err: Error, results: any) => {
                if (err){
                    console.log(err)
                } else {
                    res.json({
                        status: true,
                        data: results
                    })
                }
            })
        } else {
            res.json({
                status: false,
                message: "Presentaiton not found"
            })
        }
    }

    public static async SavePresentation(req: express.Request, res: express.Response){
        let data = req.body
        if (data.presentationID != null){
            await MysqlManager.Instance.MysqlPoolConnections.query('UPDATE  PRESENTATION SET ? WHERE ID_PRESENTATION = ?', [
                {
                    'DATEMODIFY': new Date(),
                    'NAME': data.name,
                    'STATUS': data.status,
                    'SLIDES': JSON.stringify(data.slides),
                    'ID_SLIDE': data.activeSlide,
                    'ID_USER': data.userID
                },
                data.presentationID
            ], (err: Error, results: any) => {
                if (err){
                    console.log(err)
                } else {
                    return res.json({
                        status: true,
                        results
                    })
                }
            })
        } else {
            await MysqlManager.Instance.MysqlPoolConnections.query('INSERT INTO PRESENTATION SET ?', {
                'NAME': data.name,
                'DATEMODIFY': new Date(),
                'CREATEDDATE': new Date(),
                'STATUS': data.status,
                'SLIDES': JSON.stringify(data.slides),
                'ID_SLIDE': data.activeSlide,
                'ID_USER': data.userID
            }, (err: Error, results: any) => {
                if (err){
                    console.log(err)
                } else {
                    console.log(results)
                    return res.json({
                        status: true,
                        presentationID: results.insertId
                    })
                }
            })
        }
    }

    public static async GetVote(req: express.Request, res: express.Response){
        let presentationID = req.params.id
        if (presentationID) {
            await MysqlManager.Instance.MysqlPoolConnections.query('SELECT * FROM `PRESENTATION` WHERE `ID_PRESENTATION` = ?', presentationID, (err: Error, results: any) => {
                if (err){
                    console.log(err)
                } else {
                    let data = results[0]
                    let CurrentSlide = data.ID_SLIDE
                    let slides = JSON.parse(data.SLIDES).filter((item: any) => item.id == CurrentSlide)
                    res.render('pages/answer', {
                        layout: 'layouts/default',
                        info: slides,
                        presentId: presentationID,
                        CurrentSlide: CurrentSlide
                    }, ((err, html) => WebManager.Instance.OnRequestHaveRender(req, res, err, html)))
                }
            })
        } else {
            res.json({
                status: false,
                message: "Presentation not found"
            })
        }
    }

    public static async SaveVote(req: express.Request, res: express.Response){
        let data = req.body
        if (data){
            await MysqlManager.Instance.MysqlPoolConnections.query('INSERT INTO PRESENTATION_ANSWER SET ?', {
                'ID_SLIDE': data.id_slide,
                'ID_QUESTION': data.id_question,
                'ANSWER': data.answer,
                'ID_PRESENTATION': data.id_presentation,
                'CHARTS': data.charts
            }, (err: Error, results: any) => {
                if (err){
                    console.log(err)
                } else {
                    return res.json({
                        status: true,
                        results
                    })
                }
            })
        } else {
            res.json({
                status: false,
                message: "Data not found"
            })
        }
    }

    public static async GetResults(req: express.Request, res: express.Response){
        let data = req.body

        /*
        *  id_slide = ID Активного слайда
        *  id_presentation = ID презентации
        * */
        if (data) {

            await MysqlManager.Instance.MysqlPoolConnections.query('SELECT ANSWER, count(ANSWER), CHARTS FROM `PRESENTATION_ANSWER` WHERE ID_SLIDE = ? AND ID_PRESENTATION = ?  GROUP BY ANSWER', [
                data.id_slide,
                data.id_presentation
            ], (err: Error, results: any) => {
                if (err){
                    console.log(err)
                    return res.json({
                        status: false,
                        err
                    })
                } else {
                    let data: any = []
                    let labels: any = []
                    let wc: any = []
                    results.forEach((item: any) => {
                        if (item.CHARTS === 'cloud'){
                            wc.push({
                                name: item.ANSWER,
                                value: item['count(ANSWER)'],
                                charts: item.CHARTS
                            })
                        } else {
                            data.push(item['count(ANSWER)'])
                            labels.push(item.ANSWER)
                        }
                    })
                    if (wc.length > 0){
                        return res.json({
                            status: true,
                            type: "wc",
                            data: wc
                        })
                    } else {
                        return res.json({
                            status: true,
                            type: "any",
                            data: {
                                data: data,
                                labels: labels
                            }
                        })
                    }
                }
            })

        } else {
            res.json({
                status: false,
                message: "Data not found"
            })
        }
    }


}

export default SlidesController