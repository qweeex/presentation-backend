import express from "express";
import Users from "../models/UsersModel";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import {secret} from "../data/Config.json"

class AuthController {

    public static async RegistrationUser(req: express.Request, res: express.Response){

        const {email, password, name} = req.body
        console.log(req.body)

        if (email != '' && password != ''){
            try {
                await Users.findUser(email).then(async (currentUser) => {
                    if (currentUser.length === 0){
                        const hashPassword = bcrypt.hashSync(password, 10)
                        await Users.createUser({
                            EMAIL: email,
                            PASSWORD: hashPassword,
                            USER_NAME: name
                        })
                            .then((newUser) => {
                                return res.json({
                                    status: true,
                                    message: "Регистрация успешна",
                                    newUser
                                })
                            })
                    } else {
                        return res.json({
                            status: false,
                            message: "Пользователь с таким email уже существует",
                        })
                    }
                })
            } catch (e) {
                console.error(e)
                return res.json({
                    status: false,
                    message: "Что то пошло не так...",
                    error: e
                })
            }
        } else {
            res.json({
                status: false,
                message: 'Username or password is empty'
            })
        }
    }

    public static async LoginUser(req: express.Request, res: express.Response){
        try {
            const {email, password} = req.body

            await Users.findUser(email)
                .then(user => {
                    console.log(user)
                    if (user.length > 0){
                        const validPassword = bcrypt.compareSync(password, user[0].PASSWORD)
                        if (!validPassword){
                            return res.json({
                                status: false,
                                message: "Ошибка пароля"
                            })
                        }
                        const token = jwt.sign({
                            userID: user[0].ID_USER,
                            userName: user[0].USER_NAME
                        }, secret, {
                            expiresIn: "10h"
                        })
                        return res.json({
                            status: true,
                            token: token,
                            userID: user[0].ID_USER,
                            userName: user[0].USER_NAME
                        })

                    } else {
                        return res.json({
                            status: false,
                            message: "Пользователь не найден"
                        })
                    }
                })
        } catch (e) {
            console.error(e)
            return res.json({
                status: false,
                error: e
            })
        }
    }


}

export default AuthController