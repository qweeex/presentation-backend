if(document.querySelector('#constructor')){
    var charts = null
    window.constructors = new Vue({
        el: "#constructor",
        data: {
            name: "Презентация",
            status: "active",
            activeSlide: 0,
            presentationID: null,
            slides: [],
            showSlide: [],
            charts: null,
            chartsConfig: null,
            chartsData: [],
            loading: false
        },
        created(){
            if (window.presetation !== undefined){
                this.presentationID = presetation.ID_PRESENTATION
                this.name = presetation.NAME
                this.activeSlide = presetation.ID_SLIDE
                this.slides = JSON.parse(presetation.SLIDES)
                this.initProject()
            }
        },
        mounted(){
            if (this.presentationID != null){
                this.getDataFromSlides()
                setInterval(() => this.getDataFromSlides(), 3000)
            }

        },
        methods: {
            initProject(){
                this.showSlide = this.slides.filter((slide) => slide.id === this.activeSlide)[0]

                this.chartsConfig = this.getChartsConfig(this.showSlide.chartsId)

                let labels = []
                let data = []
                this.showSlide.answers.forEach((item) => {
                    labels.push(item.text)
                    data.push(0)
                })

                this.renderCharts(data, labels)


            },
            async getDataFromSlides(){
                try {
                    let res = await axios.post('/api/results', {
                        id_slide: this.activeSlide,
                        id_presentation: this.presentationID
                    })
                    if (res.data.status && res.data.data.length > 0){
                        let dataCharts = []
                        let labelCharts = []
                        res.data.data.forEach((item) =>{
                            dataCharts.push(item.y)
                            labelCharts.push(this.showSlide.answers.filter((ans) => ans.id === item.x)[0].text)
                        })
                    }
                } catch (e){
                    console.log(e)
                }
            },
            addDataFromQuestionInput(){
                let labels = []
                this.showSlide.answers.forEach((item) => {
                    labels.push(item.text)
                })

                this.renderCharts(this.chartsData, labels)
            },
            addQuestion(){
                this.showSlide.answers.push(this.addAnswer(''))
                this.chartsData.push(0)
                this.updateDataCharts()
            },
            setSelect(e){
                this.showSlide.chartsId = e.target.value
                this.chartsConfig = this.getChartsConfig(e.target.value)

                let labels = []
                this.showSlide.answers.forEach((item) => {
                    labels.push(item.text)
                })

                this.renderCharts(this.chartsData, labels)
            },
            changeNamePresentation(e){
              this.name = e.target.innerText
            },
            dropSlides(id) {
                this.slides.filter((slide) => slide.id !== id)
            },
            addSlide() {
                let activeId = "id" + Math.random().toString(16).slice(2)
                this.activeSlide = activeId
                let answersPrev = []
                for (let i = 0; i < 3; i++){
                    answersPrev.push(this.addAnswer(''))
                    this.chartsData.push(0)
                }
                this.slides.push({
                    id: activeId,
                    question: "",
                    chartsId: "",
                    answers: answersPrev
                })
                this.showSlide = this.slides.filter((slide) => slide.id === this.activeSlide)[0]
            },
            selectSlide(id) {
                charts.destroy()

                this.activeSlide = id
                this.showSlide = this.slides.filter((slide) => slide.id === this.activeSlide)[0]
                let data = []
                let labels = []
                this.showSlide.answers.forEach((item) => {
                    data.push(0)
                    labels.push(item.text)
                })
                this.renderCharts(data, labels)
            },
            addAnswer(question){
                return {
                    id: "id" + Math.random().toString(16).slice(2),
                    text: question
                }
            },
            getChartsConfig(type){
                switch (type){
                    case 'bar':
                        return {
                            chart: {
                                id: 'mychart',
                                type: 'bar'
                            },
                            series: [{
                                name: 'sales',
                                data: []
                            }],
                            xaxis: {
                                categories: []
                            }
                        }
                        break;
                    case 'pie':
                        return  {
                            chart: {
                                id: 'mychart',
                                type: 'donut'
                            },
                            series: [],
                            labels: []
                        }
                        break;
                    case 'area':
                        return {
                            chart: {
                                id: 'mychart',
                                type: "area"
                            },
                            dataLabels: {
                                enabled: false
                            },
                            series: [
                                {
                                    name: "Series 1",
                                    data: []
                                }
                            ],
                            fill: {
                                type: "gradient",
                                gradient: {
                                    shadeIntensity: 1,
                                    opacityFrom: 0.7,
                                    opacityTo: 0.9,
                                    stops: [0, 90, 100]
                                }
                            },
                            xaxis: {
                                categories: []
                            }
                        }
                        break;
                    case 'radar':
                        return {
                            series: [{
                                name: 'Series 1',
                                data: [],
                            }],
                            chart: {
                                id: 'mychart',
                                height: 350,
                                type: 'radar',
                            },
                            xaxis: {
                                categories: []
                            }
                        }
                        break;
                    case 'polar':
                        return {
                            series: [],
                            labels: [],
                            chart: {
                                id: 'mychart',
                                type: 'polarArea',
                            },
                            stroke: {
                                colors: ['#fff']
                            },
                            fill: {
                                opacity: 0.8
                            }
                        }
                        break;
                }
            },
            renderCharts(data, labels){
                if (charts != null){
                    charts.destroy()
                }
                if (this.showSlide.chartsId === 'bar' || this.showSlide.chartsId === 'area' || this.showSlide.chartsId === 'radar') {
                    this.chartsConfig.series[0].data = data
                    this.chartsConfig.xaxis.categories = labels
                } else {
                    this.chartsConfig.series = data
                    this.chartsConfig.labels = labels
                }


                charts =  new ApexCharts(document.querySelector('#chart'), this.chartsConfig);
                charts.render()

            },
            updateDataCharts(){
                let labels = []
                this.showSlide.answers.forEach((item) => {
                    labels.push(item.text)
                })

                this.renderCharts(this.chartsData, labels)
            },
            async savePresentation(){
                this.loading = true
                try {
                    let response = await axios.post('/api/presentation/save', {
                        name: this.name,
                        status: this.status,
                        activeSlide: this.activeSlide,
                        presentationID: this.presentationID,
                        slides: this.slides
                    })
                    if (response.data.status){
                        this.presentationID = response.data.presentationID
                    }
                    this.loading = false
                } catch (e) {
                    console.log(e)
                    this.loading = false
                }
            }
        }
    })
}