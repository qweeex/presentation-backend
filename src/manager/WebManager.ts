import express from "express"
import * as core from "express-serve-static-core"
import { createServer } from "http";
import { Server } from "socket.io";
import cors from "cors"
import expressLayouts from "express-ejs-layouts"
import SlidesController from "../controller/SlidesController";
import AuthController from "../controller/AuthController";
import path from "path";
import fs from "fs";
import CheckAuth from "../middleware/CheckAuth";


class WebManager {
    public static readonly Instance: WebManager = new WebManager()
    public PORT:number = 8089;
    private readonly App!: core.Express
    public io: Server
    private server!: any

    private constructor() {
        this.App = express()
        this.App.use(express.json())
        this.App.use(expressLayouts)
        this.App.disable('x-powered-by')
        this.App.set("view engine", "ejs");
        this.App.set('layout', 'layouts/default');
        this.App.use(cors())
        this.server = createServer(this.App)
        this.io = new Server(this.server, {
            cors: {
                origin: '*',
            }
        })

        this.ApiRouter()
    }

    private ApiRouter(): void {
        // Auth API
        this.App.post('/api/auth/login', AuthController.LoginUser)
        this.App.post('/api/auth/registration', AuthController.RegistrationUser)

        this.App.post('/api/presentation', CheckAuth(),  SlidesController.GetAllPresentation) // Сохранение презентации
        this.App.post('/api/presentation/save', CheckAuth(),  SlidesController.SavePresentation) // Сохранение презентации
        this.App.post('/api/presentation/:id', CheckAuth(),  SlidesController.GetPresentation) // Получить презентацию по ID

        this.App.get('/preview/:id', SlidesController.GetVote) // Ответы на вопросы
        this.App.post('/preview/save', SlidesController.SaveVote) // Передача ответа на вопрос
        this.App.post('/api/results', SlidesController.GetResults)

        this.App.use('*', WebManager.OnErrorRouteRequest)
    }

    public Start(): void{
        this.server.listen(this.PORT, () => {
            console.log(`Server start on port ${this.PORT}`)
        })

        this.io.on("connect", (socket: any) => {
            console.log("Connected client on port %s.", this.PORT);
            socket.on("message", (m:any) => {
                console.log("[server](message): %s", JSON.stringify(m));
                this.io.emit("message", m);
            });

            socket.on('init', (m: any) => console.log(m))

            socket.on("disconnect", () => {
                console.log("Client disconnected");
            });
        });
    }

    private static OnErrorRouteRequest(req: express.Request, res: express.Response): void{
        const url = decodeURI(req.baseUrl)
        const currentPath = path.resolve('./static' + url)
        if (fs.existsSync(currentPath)) {
            res.sendFile(currentPath)
        } else {
            res.status(404).send('err 404')
        }
    }

    public OnRequestHaveRender(req: express.Request, res: express.Response, err: Error, html: string): void {
        if (err) {
            console.log(err)
            res.render("pages/errors/500", {
                layout: 'layouts/error',
            }, (errFinish, HtmlFinish) => {
                if (errFinish) {
                    res.statusCode = 500
                    res.end("Fatal error, please again later!" + "<!--" + String(err) + "-->")
                }
                res.end(HtmlFinish + "<!--" + String(err) + "-->")
            })
        } else {
            res.end(html)
        }
    }

}

export default WebManager