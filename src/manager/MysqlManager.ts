// @ts-ignore
import {createPool, Pool} from "mysql"
import MysqlDatabaseSettingLineType from "../struct/MysqlDatabaseSettingLineType";


class MysqlManager {

    public static readonly Instance: MysqlManager = new MysqlManager()
    public MysqlPoolConnections: Pool
    private MysqlCurrentSetting: MysqlDatabaseSettingLineType;

    private constructor() {
        // https://splinter.beget.com/phpMyAdmin
        this.MysqlCurrentSetting = {
            host: 'slygerev.beget.tech',
            user: 'slygerev_bd',
            pass: '3J7m%xj0',
            base: 'slygerev_bd'
        }

        this.MysqlPoolConnections = createPool({
            connectionLimit: 10,
            host: this.MysqlCurrentSetting.host,
            user: this.MysqlCurrentSetting.user,
            password: this.MysqlCurrentSetting.pass,
            database: this.MysqlCurrentSetting.base,
            charset: 'utf8mb4'
        })

        this.MysqlPoolConnections.on('error', (err: any) => {
            console.error(err)
        })

        this.MysqlPoolConnections.query('SET NAMES utf8mb4')

    }

}

export default MysqlManager