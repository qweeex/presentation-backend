import jwt from "jsonwebtoken";
import express from "express";
import {secret} from "../data/Config.json"

export default () => {
    return function (req: express.Request, res: express.Response, next: express.NextFunction) {
        if (req.method === "OPTIONS") {
            next()
        }
        try {
            if (req.headers.authorization){
                const token = req.headers.authorization.split(" ")[1]
                if (!token) {
                    return res.json({
                        status: false,
                        message: "Токен не найден"
                    })
                }
                // @ts-ignore
                const currentUser: any = jwt.verify(token, secret)
                if (currentUser.userID){
                } else {
                    return res.json({
                        status: false,
                        message: "Ошибка проверки токена"
                    })
                }
                next()
            } else {
                return res.json({status: false,message: "Ошибка проверки токена"})
            }

        } catch (error) {
            return res.json({status: false,message: "Глобальная ошибка проверки прав", error})
        }
    }
}